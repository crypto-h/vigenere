export declare const FIRST_CODE: number;
export declare const ALPH_LENGTH = 26;
type HashRow = string[];
type HashTable = Record<number, HashRow>;
export declare const getHashTable: () => HashTable;
export {};
