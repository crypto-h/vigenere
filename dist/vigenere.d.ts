export declare function encode(data: string, key: string): string;
export declare function decode(data: string, key: string): string;
