export const FIRST_CODE = 'a'.charCodeAt(0);
export const ALPH_LENGTH = 26;

type HashRow = string[];
type HashTable = Record<number, HashRow>;

const getHashColumn = (index: number) => {
  const row = [];
  for(let i = index; i < index + ALPH_LENGTH; i++) {
    const code = (i % ALPH_LENGTH) + FIRST_CODE;
    row.push(String.fromCharCode(code));
  }
  return row;
}

export const getHashTable = () => {
  const table: HashTable = {};
  for(let i = 0; i < ALPH_LENGTH; i++ ){
    table[FIRST_CODE + i] = getHashColumn(i);
  }
  return table;
}