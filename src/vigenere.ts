import { ALPH_LENGTH, FIRST_CODE, getHashTable } from './helpers';

export function encode(data: string, key: string) {
  const hash = getHashTable();
  let result = '';
  for(let i = 0; i < data.length; i++) {
    const keySymbol = key[i % key.length];
    const keyCode = keySymbol.charCodeAt(0);
    const hashColumn = hash[keyCode];
    const symbolCode = data[i].charCodeAt(0);

    let symbol = data[i];
    if(symbolCode >= FIRST_CODE && symbolCode < FIRST_CODE + ALPH_LENGTH) {
      symbol = hashColumn[data[i].charCodeAt(0) % FIRST_CODE];
    }
    result += symbol;
  }
  return result;
}

export function decode(data: string, key: string) {
  const hash = getHashTable();
  let result = '';
  dataLabel: for(let i = 0; i < data.length; i++) {
    const keySymbol = key[i % key.length];
    const keyCode = keySymbol.charCodeAt(0);
    const hashColumn = hash[keyCode];
    for(let j = 0; j < ALPH_LENGTH; j++) {
      if(hashColumn[j] === data[i]) {
        result += String.fromCharCode(FIRST_CODE + j);
        continue dataLabel;
      }
    }
    result += data[i];
  }
  return result;
}